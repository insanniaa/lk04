import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            // edit here
            try {

                System.out.println("Masukkan nilai pembilang : ");
                int pembilang = scanner.nextInt();

                System.out.println("Masukkan nilai penyebut : ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil : " + hasil);

                validInput = true;

            } catch (InputMismatchException e) {
                System.out.println("Nilai harus bilangan bulat");
                scanner.nextLine();

            } catch (NullPointerException e) {
                System.out.println("Penyebut tidak boleh bernilai 0(null)");
                scanner.nextLine();
            }

        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        // add exception apabila penyebut bernilai 0
        if (penyebut == 0) {
            throw new NullPointerException("Penyebut tidak boleh bernilai 0(null)");
        }
        return pembilang / penyebut;
    }
}
